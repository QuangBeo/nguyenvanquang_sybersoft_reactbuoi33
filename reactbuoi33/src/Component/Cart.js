import React, { Component } from 'react'
export default class Cart extends Component {
    renderTbody = () => {

        return this.props.cart.map((item,index) => {
            const newPrice = item.price * item.number;
            return (
                <tr key={index}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{newPrice}</td>
                    <td>
                        <button className='btn btn-secondary' onClick={()=>this.props.handleDown(item)}>-</button>
                        <span className='px-3'>{item.number}</span>
                        <button className='btn btn-secondary' onClick={()=>this.props.handleUp(item)}>+</button>
                    </td>
                    <td><img src={item.image} style={{width:"20%"}} alt="" /></td>
                </tr>
            );
        });
    };
  render() {
    return (
      <table className="table">
        <thead>
            <tr>
                <td><b>Id</b></td>
                <td><b>Name</b></td>
                <td><b>Price</b></td>
                <td><b>number</b></td>
                <td style={{width:"20%"}}><b>Image</b></td>
            </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    )
  }
}
