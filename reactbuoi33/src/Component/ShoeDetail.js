import React, { Component } from 'react'

export default class ShoeDetail extends Component {
  render() {
    let {name,price,description,image} = this.props.detail;
    return (
      <div className="row container mx-auto mt-5">
        <div className="col-4">
            <h2>Ảnh Sản Phẩm</h2>
            <img src={image} className="w-100" alt="" />
        </div>
        <div className="col-8 text-left">
            <h2>Thông Tin Chi Tiết</h2>
            <p className='mt-5'><b>Tên:</b> {name}</p>
            <p><b>Giá:</b> {price}</p>
            <p><b>Mô Tả:</b> {description}</p>
        </div>
      </div>
    )
  }
}
