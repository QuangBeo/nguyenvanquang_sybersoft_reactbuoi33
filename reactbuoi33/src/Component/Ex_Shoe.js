import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./DataShoe";
import ListShoe from "./ListShoe";
import ShoeDetail from "./ShoeDetail";

export default class Ex_Shoe extends Component {
  state = {
    ArrShoe: dataShoe,
    detail: dataShoe[0],
    cart: [],
  };
  handleDetail = (value) => {
    this.setState({
        detail: value,
    });
  };

  handleAddtoCard = (shoe) => { 
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => { 
        return item.id == shoe.id;
     })
     if(index == -1){
        let cartItem = {...shoe,number:1};
        cloneCart.push(cartItem);
     }else{
        cloneCart[index].number++;
     }
     this.setState({
        cart:cloneCart,
     })
   }
   handleUp = (item) => {
    let newCart = [...this.state.cart];
    item.number++;
    this.setState({
        cart:newCart,
     })
   }
   handleDown = (item) => {
    let newCart = [...this.state.cart];
    if(item.number > 1){
        item.number--;
    }
    this.setState({
        cart:newCart,
     })
   }
  render() {
    return <div>
        <Cart handleDown={this.handleDown} handleUp={this.handleUp} cart={this.state.cart}/>
        <ListShoe handleAddtoCard={this.handleAddtoCard} handleDetail={this.handleDetail} ArrShoe={this.state.ArrShoe}/>
        <ShoeDetail detail={this.state.detail}/>
    </div>;
  }
}
