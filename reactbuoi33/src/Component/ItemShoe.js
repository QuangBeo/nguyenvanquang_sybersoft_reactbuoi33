import React, { Component } from 'react'

export default class ItemShoe extends Component {
    render() {
    let {name,image,shortDescription} = this.props.shoe;
    return (
      <div className='item'>
        <div className="card text-left">
        <img className="card-img-top" src={image} alt />
        <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p className="card-text">{shortDescription}</p>
            <button onClick={ () => {this.props.handleAddtoCard(this.props.shoe)}} className='btn btn-danger'>Add</button>
            <button onClick={ () => {this.props.handleDetail(this.props.shoe)}} className='btn btn-success mx-3'>Detail</button>
        </div>
        </div>
      </div>
    )
  }
}





