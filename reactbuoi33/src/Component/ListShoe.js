import React, { Component } from 'react'
import ItemShoe from './ItemShoe'

export default class ListShoe extends Component {
    renderArrShoe = () => {
        return this.props.ArrShoe.map((item,index) => { 
            return <div key={index} className="col-4 p-2">
                <ItemShoe handleAddtoCard={this.props.handleAddtoCard} handleDetail={this.props.handleDetail} shoe={item}/>
            </div>
        })
    }
    render() {
    return (
      <div className="row container mx-auto">
        {this.renderArrShoe()}
      </div>
    )
  }
}
